import * as express from 'express'
import Middlewares from './config/middlewares/base/base.middleware'
import Notify from './config/notify'

const app = express()
const port = parseInt(process.env.PORT, 10) || 3001
app.use(Middlewares.configuration)

app.listen(port, (err) => {
  if (err) {
    // eslint-disable-next-line
      console.error('Start server error')
  } else {
    // eslint-disable-next-line
      console.info(
      `
              =====================================================
              -> Server 🏃 (running) on Port:${port} (${process.env.NODE_ENV})
              =====================================================
                                Spirit Framework
              =====================================================
          `
    )
    if (process.env.NODE_ENV === 'development') {
      Notify.info('Server running', `on Port:${port} (${process.env.NODE_ENV})`)
    }
  }
})

export default app
