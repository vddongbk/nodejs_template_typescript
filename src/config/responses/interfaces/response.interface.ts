import { Response } from 'express'
import { SpecificError } from '../../types'

interface ResponseInterface {
  success: (res: Response, options: { data: any, message: string }) => void
  createdSuccess: (res: Response, options: { data: any, message: string }) => void
  unauthorized: (res: Response) => void
  servereError: (res: Response) => void
  forbidden: (res: Response) => void
  notFound: (res: Response, error: SpecificError) => void
  badRequest: (res: Response, error: SpecificError) => void
  badRequestMessage: (res: Response, message: string | null) => void
}

export default ResponseInterface
