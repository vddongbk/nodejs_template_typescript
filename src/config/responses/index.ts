import { Response } from 'express'
import { SpecificError } from '../types'
import ResponseInterface from './interfaces/response.interface'
import ErrorsConstant from '../constants/errors.constant'

class HttpResponse implements ResponseInterface {
  success (res: Response, options?: { data: any, message: string }): object {
    const data = options && options.data
    const message = options && options.message

    res.status(200)

    return res.json({
      statusCode: 200,
      data: data || null,
      message: message ? res.__(message) : res.__('success'),
      code: 0
    })
  }

  createdSuccess (res: Response, options?: { data: any, message: string }): object {
    const data = options && options.data
    const message = options && options.message

    res.status(201)

    return res.json({
      statusCode: 201,
      data: data || null,
      message: message ? res.__(message) : res.__('Created success'),
      code: 0
    })
  }

  unauthorized (res: Response): object {
    return (<any>res).boom.unauthorized(res.__('Unauthorized'), {
      code: ErrorsConstant.unauthorized
    })
  }

  servereError (res: Response): object {
    return (<any>res).boom.badImplementation(res.__('An internal server error occurred'), {
      code: ErrorsConstant.serverError
    })
  }

  notFound (res: Response, error?: SpecificError): object {
    if (!error) {
      return (<any>res).boom.notFound(res.__('Not Found'), {
        code: ErrorsConstant.notFound
      })
    }
    return (<any>res).boom.notFound(res.__(error.message), {
      code: error.code
    })
  }

  forbidden (res: Response): object {
    return (<any>res).boom.forbidden(res.__('Permission Denied'), {
      code: ErrorsConstant.forbidden
    })
  }

  badRequest (res: Response, error?: SpecificError): object {
    if (!error) {
      return (<any>res).boom.badRequest(res.__('Bad Request'), {
        code: ErrorsConstant.badRequest
      })
    }
    return (<any>res).boom.badRequest(res.__(error.message), {
      code: error.code
    })
  }

  badRequestMessage (res: Response, message?: string | null): object {
    if (!message) {
      return (<any>res).boom.badRequest(res.__('Bad Request'), {
        code: ErrorsConstant.badRequest
      })
    }
    return (<any>res).boom.badRequest(res.__(message), {
      code: ErrorsConstant.badRequest
    })
  }
}

export default new HttpResponse()
