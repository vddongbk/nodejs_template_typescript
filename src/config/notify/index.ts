import * as notifier from 'node-notifier'
import * as path from 'path'


class Notify {
  error (title: string, message: string) {
    const options : any = {
      title,
      message,
      contentImage: path.join(__dirname, '../../../', './public/notify/error.png'),
      icon: path.join(__dirname, '../../../', './public/notify/error.png'),
      sound: 'Hero',
      sticky: false,
    }
    notifier.notify(options)
  }

  info (title: string, message: string) {
    const options : any = {
      title,
      message,
      contentImage: path.join(__dirname, '../../../', './public/notify/info.jpg'),
      icon: path.join(__dirname, '../../../', './public/notify/info.jpg'),
      sound: 'Hero',
      sticky: false,
    }
    notifier.notify(options)
  }
}

export default new Notify()
