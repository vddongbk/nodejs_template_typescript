import * as kue from 'kue'
import Environment from '../constants/environment.constant'
import Spirit from '../debug'

if (Environment.env === 'development') kue.app.listen(3000)

class Queue {
  public _queue
  constructor () {
    const queue = kue.createQueue({
      prefix: 'q',
      redis: {
        port: Environment.redis.port,
        host: Environment.redis.host,
        db: Environment.redis.db
      }
    })

    queue.watchStuckJobs(null)

    queue.on('error', (err) => {
      Spirit.error('There was an error in the main queue!')
      Spirit.error(err)
      Spirit.error(err.stack)
    })
    this._queue = queue
  }
}

export default Queue
