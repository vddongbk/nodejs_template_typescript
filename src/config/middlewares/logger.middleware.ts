import * as express from 'express'
import * as expressWinston from 'express-winston'
import winstonInstance from '../winston'

class LoggerMiddleware {
  static configurationLogger () : any {
    const app = express()
    // enable detailed API logging in dev env
    // if (config.env === 'development') {
    expressWinston.requestWhitelist.push('body')
    expressWinston.responseWhitelist.push('body')
    const options : any = {
      winstonInstance,
      meta: true, // optional: log meta data about request (defaults to true)
      msg: 'HTTP {{req.method}} {{req.url}} {{res.statusCode}} {{res.responseTime}}ms',
      expressFormat: true,
      colorize: true,
      colorStatus: true, // Color the status code (default green, 3XX cyan, 4XX yellow, 5XX red).
      statusLevels: false, // default value
      level (req, res) {
        let level = ''
        if (res.statusCode >= 100) { level = 'info' }
        if (res.statusCode >= 400) { level = 'warn' }
        if (res.statusCode >= 500) { level = 'error' }
        // Ops is worried about hacking attempts so make Unauthorized and Forbidden critical
        if (res.statusCode === 401 || res.statusCode === 403) { level = 'critical' }
        // No one should be using the old path, so always warn for those
        if (req.path === '/api/v1' && level === 'info') { level = 'warn' }
        return level
      }
    }
    app.use(expressWinston.logger(options))
    // }
    return app
  }

  static configurationErrorLogger () : any {
    const app = express()
    // log error in winston transports except when executing test suite
    // if (config.env !== 'test') {
    const options : any = {
      winstonInstance
    }
    app.use(expressWinston.errorLogger(options))
    // }
    return app
  }
}

Object.seal(LoggerMiddleware)
export default LoggerMiddleware
