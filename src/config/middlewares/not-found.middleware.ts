import * as express from 'express'
import ErrorsConstant from '../constants/errors.constant'

class NotFoundMiddleware {
  static configuration () : any {
    const app = express()
    app.use((req: express.Request, res: express.Response) => {
      (<any>res).boom.notFound('Not Found', { code: ErrorsConstant.notFound })
    })
    return app
  }
}

Object.seal(NotFoundMiddleware)
export default NotFoundMiddleware
