import * as methodOverride from 'method-override'
import * as express from 'express'

class MethodOverrideMiddleware {
  static configuration () : any {
    const app = express()
    app.use(methodOverride())
    return app
  }
}


Object.seal(MethodOverrideMiddleware)
export default MethodOverrideMiddleware
