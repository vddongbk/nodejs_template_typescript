import * as express from 'express'
import * as i18n from 'i18n'
import * as path from 'path'

class I18nMiddleware {
  static configuration () : any {
    const app = express()
    app.use(i18n.init)
    i18n.configure({
      locales: ['en', 'vi'],

      directory: path.join(__dirname, '../../../', './locales'),

      defaultLocale: 'en', // accept-language

      autoReload: true,

      updateFiles: true,

      syncFiles: true,

    })
    return app
  }
}

Object.seal(I18nMiddleware)
export default I18nMiddleware
