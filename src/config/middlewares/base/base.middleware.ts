import * as compress from 'compression'
import * as cors from 'cors'
import * as helmet from 'helmet'
import * as logger from 'morgan'
import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as path from 'path'
import * as boom from 'express-boom'
import * as dotenv from 'dotenv'
import * as lusca from 'lusca'
import NotFoundMiddleware from '../not-found.middleware'
import I18nMiddleware from '../i18n.middleware'
import MethodOverrideMiddleware from '../method-override.middleware'
import BaseRoutes from '../../routes/base/base.routes'
import LoggerMiddleware from '../logger.middleware'

class BaseMiddleware {
  static get configuration () {
    const app = express()
    const pathStatic = path.join(__dirname, '../../../../', 'public')

    app.use(I18nMiddleware.configuration())
    app.use(logger('dev'))
    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({ extended: true }))
    app.use(express.static(pathStatic, { maxAge: 31557600000 }))
    app.use(boom())
    app.use(compress())
    app.use(MethodOverrideMiddleware.configuration())
    app.use(helmet())
    app.use(lusca.xframe('SAMEORIGIN'))
    app.use(lusca.xssProtection(true))
    app.use(cors({ origin: '*' }))
    app.use(LoggerMiddleware.configurationLogger())
    app.use(new BaseRoutes().routes)
    app.use(NotFoundMiddleware.configuration())
    app.use(LoggerMiddleware.configurationErrorLogger())
    dotenv.config()

    return app
  }
}

Object.seal(BaseMiddleware)
export default BaseMiddleware
