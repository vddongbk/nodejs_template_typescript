import { string, number, validate, object, boolean } from 'joi'
import * as dotenv from 'dotenv'

dotenv.config()

// define validation for all the env vars
const envVarsSchema = object({
  NODE_ENV: string().allow(['development', 'production', 'staging']).default('development'),
  PORT: number().default(3001),
  MONGODB_URI: string().required(),
  JWT_SECRET: string().required().description('JWT Secret required to sign'),
  JWT_EXPIRE_TIME: string().default('7 days'),
  REDIS_HOST: string().required(),
  REDIS_PORT: number().default(6379),
  REDIS_DB: number().default(1),
  REDIS_PREFIX: string().default('my_key_'),
  // GMAIL
  GMAIL_USERNAME: string().required(),
  GMAIL_PASSWORD: string().required(),
  // SES
  SES_ACCESS_KEY_ID: string().required(),
  SES_SECRET_ACCESS_KEY: string().required(),
  SES_REGION: string().default('us-west-2'),
  FB_URL: string().required(),
  GG_URL: string().required(),
  ZALO_URL: string().required(),
  // WINSTON LOGSTASH
  WINSTON_LOGSTASH_PORT: number().default(7000),
  WINSTON_LOGSTASH_HOST: string().required(),
  WINSTON_SSL_ENABLE: boolean().default(false),
  WINSTON_MAX_CONNECT_RETRIES: number().default(-1),
  WINSTON_LOGSTASH_NODE: string().required(),
}).unknown().required()

const { error, value: envVars } = validate(process.env, envVarsSchema)
if (error) {
  throw new Error(`Config validation error: ${error.message}`)
}

class EnvironmentConstant {
    static DB_CONNECTION_STRING: string = envVars.MONGODB_URI
    static env: string = envVars.NODE_ENV
    static port: string = envVars.PORT
    static jwtSecret: string = envVars.JWT_SECRET
    static jwtExpireTime: string = envVars.JWT_EXPIRE_TIME
    static redis: any = {
      host: envVars.REDIS_HOST,
      port: envVars.REDIS_PORT,
      db: envVars.REDIS_DB,
      prefix: envVars.REDIS_PREFIX
    }
    static gmail: object = {
      user: envVars.GMAIL_USERNAME,
      pass: envVars.GMAIL_PASSWORD
    }
    static ses: any = {
      accessKeyId: envVars.SES_ACCESS_KEY_ID,
      secretAccessKey: envVars.SES_SECRET_ACCESS_KEY,
      region: envVars.SES_REGION
    }
    static domain: string = envVars.DOMAIN
    static fbUrl: string = envVars.FB_URL
    static ggUrl: string = envVars.GG_URL
    static zaloUrl: string = envVars.ZALO_URL
    static logStash: object = {
      port: envVars.WINSTON_LOGSTASH_PORT,
      ssl_enable: envVars.WINSTON_SSL_ENABLE,
      host: envVars.WINSTON_LOGSTASH_HOST,
      max_connect_retries: envVars.WINSTON_MAX_CONNECT_RETRIES,
      node_name: envVars.WINSTON_LOGSTASH_NODE,
    }
}
Object.seal(EnvironmentConstant)
export default EnvironmentConstant

