import { SpecificError } from '../types'

class ErrorsConstant {
    // common errors
    static validate: number = 1
    static badRequest: number = 2
    static serverError: number = 3
    static notFound: number = 4
    static unauthorized: number = 5
    static forbidden: number = 6

    // specific errors
    static emailNotExists: SpecificError = {
      message: 'email not exists',
      code: 10,
    }

    static typeNotAllows: SpecificError = {
      message: 'type not allows',
      code: 12
    }

    static fileTooLarger: SpecificError = {
      message: 'file too larger',
      code: 13
    }

    static errorhandleUpload: SpecificError = {
      message: 'Error in-progress upload',
      code: 14
    }
}
Object.seal(ErrorsConstant)
export default ErrorsConstant
