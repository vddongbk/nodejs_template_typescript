import * as debug from 'debug'

debug.disable()
debug.enable('Spirit:*')

class Debug {
    static log:debug.IDebugger = debug('Spirit:log');
    static error:debug.IDebugger = debug('Spirit:error');
}

/* eslint-disable no-console */
Debug.log.log = console.log.bind(console)
Debug.error.log = console.error.bind(console)

export default Debug
