import * as express from 'express'
import * as expressValidation from 'express-validation'
import UserRoutes from '../user.routes'
import Constants from '../../constants/environment.constant'
import ErrorsConstant from '../../constants/errors.constant'
import AuthRoutes from '../auth.routes'
import Spirit from '../../debug'
import Notify from '../../notify'

const app = express()
const prefix = '/api/v1'

class BaseRoutes {
  get routes () {
    app.use(prefix, new UserRoutes().routes)
    app.use(prefix, new AuthRoutes().routes)

    app.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
      Spirit.error(err)
      if (err instanceof expressValidation.ValidationError) {
        const firstError = err.errors[0].messages[0].replace(/"/g, '')
        // validation error contains errors which is an array of error each containing message[]
        if (Constants.env === 'development') {
          Notify.error('Error Validation!', firstError)
          const unifiedErrorMessage = err.errors.map((error) => error.messages
            .join('. '))
            .join(' and ')
            .replace(/"/g, '')
          return (<any>res).boom.badData(res.__(firstError),
            { code: ErrorsConstant.validate, devMessages: unifiedErrorMessage })
        }
        return (<any>res).boom.badData(res.__(firstError), { code: ErrorsConstant.validate })
      }

      if (err && err.name === 'ValidationError') {
        const firstError = err.details[0].message.replace(/"/g, '')
        if (Constants.env === 'development') {
          Notify.error('Error Validation!', firstError)
          const unifiedErrorMessage = err.details.map((error) => error.message)
            .join(' and ')
            .replace(/"/g, '')
          return (<any>res).boom.badData(res.__(firstError),
            { code: ErrorsConstant.validate, devMessages: unifiedErrorMessage })
        }
        return (<any>res).boom.badData(res.__(firstError), { code: ErrorsConstant.validate })
      }

      if (err) {
        if (Constants.env === 'development') { Notify.error('Error!!!', null) }
        return (<any>res).boom.badRequest(res.__('Bad Request'), { code: ErrorsConstant.badRequest })
      }

      return next()
    })
    return app
  }
}
export default BaseRoutes
