import * as express from 'express'
import AuthController from '../../app/controllers/auth.controller'
import asyncWrap from '../../app/policies/middlewares/async-wrap.middleware'
import AuthValidation from '../../app/policies/validations/auth.validation'

const router : express.Router = express.Router()
class AuthRoutes {
    private readonly _authController: AuthController;

    constructor () {
    this._authController = new AuthController()
  }

    get routes () {
    const controller = this._authController
    router.post('/login', AuthValidation.login, asyncWrap(controller.login))
    router.get('/health_check', asyncWrap(controller.healthCheck))

    return router
  }
}

Object.seal(AuthRoutes)
export default AuthRoutes
