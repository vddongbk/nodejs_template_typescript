import * as express from 'express'
import UserController from '../../app/controllers/user.controller'
import asyncWrap from '../../app/policies/middlewares/async-wrap.middleware'
import AuthenticationMiddleware from '../../app/policies/middlewares/authentication.middleware'

const router : express.Router = express.Router()
class UserRoutes {
  private readonly _userController: UserController;

  constructor () {
    this._userController = new UserController()
  }

  get routes () {
    const controller = this._userController
    router.get('/users', AuthenticationMiddleware.authentication, asyncWrap(controller.retrieve))
    router.post('/users', AuthenticationMiddleware.authentication, asyncWrap(controller.create))
    router.put('/users/:_id', AuthenticationMiddleware.authentication, asyncWrap(controller.update))
    router.get('/users/:_id', AuthenticationMiddleware.authentication, asyncWrap(controller.findById))
    router.delete('/users/:_id', AuthenticationMiddleware.authentication, asyncWrap(controller.delete))

    return router
  }
}

Object.seal(UserRoutes)
export default UserRoutes
