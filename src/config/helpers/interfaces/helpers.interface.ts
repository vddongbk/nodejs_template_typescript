interface HelpersInterface {
    randomNumber: (length: number) => void
    getIpAddress: () => void
    getDevice: (header: any) => void
    getSkipItemByPage: (page: number, limit: number) => void
    calculatorTotalPages: (totalDocument: number, limiy: number) => void
    isMongoId: (id: string) => void
    toObjectId: (id: string) => void
    optionPaginateLean: (page: number, populate: any, sort: object, limit: number) => void
    transferToArrayValue: (arrayObject: any, property: string) => void
    generateError: ({ message: string, code: number }) => void
    optionPaginate: (page: number, selectFields: string, populate: any, sort: object, limit: number) => void
    getNameImage: (urlImage: string) => void
    getNumberItemLoad: (page: number, arrayLength: number, limit: number) => void
    minuteFromNow: (startTime: string) => void
    now: () => void
    isNumber: (number: number) => void
    getPage: (page: number) => void
    randomImage: (name: string) => void
    getMailLimitKey: (email: string) => void
    getLimitTwoArray: (arr1: any, arr2: any) => void
    filterExtension: (typeFile: string) => void
}

export default HelpersInterface
