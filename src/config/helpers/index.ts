import * as ip from 'ip'
import * as MobileDetect from 'mobile-detect'
import * as mongoose from 'mongoose'
import * as moment from 'moment'
import * as faker from 'faker'
import HelpersInterface from './interfaces/helpers.interface'

const objectId = mongoose.Types.ObjectId

class Helpers implements HelpersInterface {
  randomNumber (length: number = 6) {
    if (length > 15) length = 15
    let result = ''
    for (let i = 0; i < length; i += 1) {
      const randomNumber = Math.floor(Math.random() * 10)
      result += String(randomNumber)
    }
    return result
  }

  getIpAddress () {
    const IP = ip.address()
    return IP
  }

  getDevice (header: any) {
    const detect = new MobileDetect(header)
    const device = detect.os()
    return device || 'undefined'
  }

  getSkipItemByPage (page: number = 1, limit: number = 10) {
    const skip = ((page - 1) * limit)
    return skip
  }

  calculatorTotalPages (totalDocument = 0, limit = 10) {
    const total = Math.ceil(totalDocument / limit)
    return total
  }

  isMongoId (id: string) {
    const isMongoId = objectId.isValid(id)
    return isMongoId
  }

  toObjectId (id: string) {
    const ObjectID = objectId(id)
    return ObjectID
  }

  optionPaginateLean (page: number = 1, populate: any, sort: object, limit: number = 10) {
    const paginate = {
      lean: true,
      sort,
      populate,
      limit,
      page
    }
    return paginate
  }

  transferToArrayValue (arrayObject: any, property: string = '_id') {
    const outputArray = []
    arrayObject.forEach((object) => {
      if (object[property] !== undefined) outputArray.push(object[property])
    })
    return outputArray
  }

  generateError (data) {
    const { message, code } = data
    const error = new Error(message)
    if (code) (<any>error).code = code
    return error
  }

  optionPaginate (page: number = 1, selectFields: string, populate: any, sort: object = { createdAt: -1 },
    limit: number = 10) {
    const paginate = {
      sort,
      select: selectFields,
      page,
      limit,
      populate
    }
    return paginate
  }

  getNameImage (urlImage: string) {
    const pathArr = urlImage.split('/')
    const name = pathArr[pathArr.length - 1]
    return name
  }

  getNumberItemLoad (page: number, arrayLength: number, limit: number = 10) {
    let numberItems = limit * page
    if (numberItems > arrayLength) {
      numberItems = arrayLength
    }
    return numberItems
  }

  minuteFromNow (startTime: string) {
    try {
      const end = moment()
      const duration = moment.duration(end.diff(startTime))
      return duration.asMinutes()
    } catch (error) {
      throw error
    }
  }

  now () {
    try {
      return moment().format()
    } catch (error) {
      throw error
    }
  }

  isNumber (number: number) {
    let valid = true
    if (number === null) {
      valid = false
    } else if (number === undefined) {
      valid = false
    } else if (!Number(number)) {
      valid = false
    }
    return valid
  }

  getPage (page: number) {
    if (!page || page < 0 || isNaN(page)) {
      const newPage = 1
      return newPage
    }
    return page
  }

  randomImage (name: string) {
    switch (name) {
      case 'avatar':
        return faker.image.avatar()
      case 'food':
        return faker.image.food()
      case 'business':
        return faker.image.business()
      case 'transport':
        return faker.image.transport()
      default:
        return faker.image.fashion()
    }
  }

  getMailLimitKey (email: string) {
    return `reset_password_limit_${email}`
  }

  getLimitTwoArray (arr1: any, arr2: any) {
    if (!Array.isArray(arr1) || !Array.isArray(arr2)) return 1
    const length1 = arr1.length
    const length2 = arr2.length
    if (length1 >= length2) return length2
    return length1
  }

  filterExtension (typeFile: string) {
    const type = typeFile.split('/')
    let extension = type[1]
    switch (extension) {
      case 'vnd.openxmlformats-officedocument.presentationml.presentation':
        extension = 'pptx'
        break
      default:
        break
    }
    return extension
  }
}

export default new Helpers()
