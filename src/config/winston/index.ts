import { createLogger, format, transports } from 'winston'
// import * as fs from 'fs'
import * as path from 'path'
// import Environment from '../constants/environment.constant'
import 'winston-logstash'

class WinstonInstance {
  get logger () {
    const logger = createLogger({
      format: format.combine(
        format.timestamp(),
        format.simple()
      ),
      transports: [
        new transports.Console({
          format: format.combine(
            format.timestamp(),
            format.colorize({ all: true }),
            format.simple()
          )
        }),
        // new transports.Stream({
        //   stream: fs.createWriteStream(path.join(__dirname, '../../../', './server.log'))
        // })
        new transports.File({
          filename: path.join(__dirname, '../../../', './server.log')
        })
      ]
    })

    // if (Environment.env !== 'development') {
    //   logger = createLogger({
    //     transports: [
    //       new ((<any>transports).logStash)(Environment.logStash)
    //     ]
    //   })
    // }
    return logger
  }
}

export default new WinstonInstance().logger
