import { NextFunction, Request, Response } from 'express'

export type SpecificError = { message: string, code: number } | null
export type errorCallback = (req: Request, res: Response, error: any) => void | null
export type fn = (req: Request, res: Response, next: NextFunction) => Promise<any>
export type uploadOptions = { types: any, maxSize: number, isArray: boolean, typeUpload: string }
export type optionGeoSearch = { near: [number, number], maxDistance: number, limit: number, lean: boolean }
