import * as faker from 'faker'
import * as _ from 'lodash'
import * as moment from 'moment-timezone'
import UserDataSeed from './data/user.seed'
import UserBusiness from '../app/business/user.business'
import UserModelInterface from '../app/model/interfaces/user-model.interface'

moment.tz.setDefault('Asia/Ho_Chi_Minh')

class HelperSeed {
  static randomImage (name): void {
    switch (name) {
      case 'avatar':
        return faker.image.avatar()
      case 'food':
        return faker.image.food()
      case 'business':
        return faker.image.business()
      default:
        return faker.image.fashion()
    }
  }

  static getArrayImages (name): any {
    const images = []
    const number = faker.random.number({ min: 1, max: 10 })
    for (let i = 0; i < number; i += 1) {
      images.push({
        origin: this.randomImage(name),
        thumb: this.randomImage(name)
      })
    }
    return images
  }

  static async kickoff (tasks): Promise<any> {
    await tasks.run()
    process.exit()
  }

  static async fakerUser (): Promise<any> {
    try {
      const heroBusiness = new UserBusiness()
      const data : UserModelInterface = <UserModelInterface>UserDataSeed.users()
      await heroBusiness.create(data, null)
      return true
    } catch (error) {
      throw error
    }
  }

  static async removeUsers (): Promise<any> {
    try {
      const heroBusiness = new UserBusiness()
      await heroBusiness.remove({})
      return true
    } catch (error) {
      throw error
    }
  }

  static destroyDB (): any {
    return [
      {
        title: 'Remove user collection',
        task: async (ctx, task) => {
          task.output = 'Remove user collection'
          await HelperSeed.removeUsers()
        }
      }
    ]
  }

  /**
  * Load data into MongoDB
  *
  * This method deletes existing data before importing the
  * samples. Be careful here, there’s no approval question
  * before deletion.
  *
  * @return {Array} tasks for listr
     */
  static pumpItUp () {
    return _.concat(
      // add task to remove data before import to avoid errors
      this.destroyDB(),

      // the actual tasks to import data
      [
        {
          title: 'Create user test simple for user model 👌',
          task: async (ctx, task) => {
            task.output = 'Importing user test'
            await this.fakerUser()
          }
        },
      ]
    )
  }
}

Object.seal(HelperSeed)
export default new HelperSeed()
