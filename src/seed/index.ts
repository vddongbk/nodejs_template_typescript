import * as express from 'express'
import * as Listr from 'listr'
import Middlewares from '../config/middlewares/base/base.middleware'
import HelperSeed from './helpers'

const app = express()
const port = parseInt(process.env.PORT_SEED, 10) || 3002
app.use(Middlewares.configuration)

app.listen(port, (err) => {
  if (err) {
    // eslint-disable-next-line
      console.error('Start server to seed error')
  } else {
    // eslint-disable-next-line
      console.info(
      `
              =====================================================
              -> Start seed 🏃 (running) on Port:${port} (${process.env.NODE_ENV})
              =====================================================
                                Spirit Framework
              =====================================================
          `
    )
  }
})

/**
 * Entry point for the NPM "pumpitup" and "cleanup" scripts
 */
if ((<any>process).argv.includes('--destroy')) {
  const cleanUp = (<any>HelperSeed).destroyDB();
  (<any>HelperSeed).kickoff(new Listr(cleanUp))
} else {
  const pumpIt = (<any>HelperSeed).pumpItUp();
  (<any>HelperSeed).kickoff(new Listr(pumpIt))
}
