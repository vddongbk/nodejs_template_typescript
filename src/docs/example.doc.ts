export = {
  /**
     * @api {post} /auth/login Login
     * @apiVersion 1.0.0
     * @apiPermission user, driver
     * @apiName Login
     * @apiGroup AUTH
     *
     * @apiParam {String} [email] Email of the User.<br><code>required malformed</code>
     * @apiParam {String} [password] Password of the User.<br><code>required min:6 max:255</code>
     * @apiParam {String} [deviceToken] Device token that user using.<br><code>required</code>
     * @apiParam {String} [deviceType] Device type that user using.<br><code>required</code>
     * @apiParamExample {json} Request-Example:
     * {
     *   "email": "d2d@d2d.net",
     *   "password": "123123",
     *   "deviceToken": "7apsIGEwpUFgj6yL5nYuXRWj2rnP9UrzK91w05mRxXGvx5QqQkj0gWZUHN44L9jwkHerkEDp",
     *   "deviceType" "ios",
     * }
     *
     * @apiSuccess {Number} [statusCode=200] status response.
     * @apiSuccess {String} [data=token] data response.
     * @apiSuccess {String} [message="Success"] message success.
     * @apiSuccess {Number} [code=0] value code.
     * @apiSuccessExample Success Response:
     * {
     *   "statusCode": 200,
     *   "data": "7apsIGEwpUFgj6yL5nYuXRWj2rnP9UrzK91w05mRxXGvx5QqQkj0gWZUHN44L9jwkHerkEDp",
     *   "message": "Success",
     *   "error": 0
     * }
     *
     * @apiError {Number} [statusCode=400,422,500] status response.
     * @apiError {Boolean} error message error common http-request.
     * @apiError {String} message message error response.
     * @apiError {Number=1,2,3,10,11} [code=1,2,3,10,11] value code.
     * @apiErrorExample Error email required:
     * {
     *   "statusCode": 422,
     *   "error": "Unprocessable Entity",
     *   "message": "\"email\" is required",
     *   "code": 1
     * }
     * @apiErrorExample Error email is not allowed to be empty:
     * {
     *   "statusCode": 422,
     *   "error": "Unprocessable Entity",
     *   "message": "\"email\" is not allowed to be empty",
     *   "code": 1
     * }
     * @apiErrorExample Error email must be a valid email:
     * {
     *   "statusCode": 422,
     *   "error": "Unprocessable Entity",
     *   "message": "\"email\" must be a valid email",
     *   "code": 1
     * }
     * @apiErrorExample Error email not exist:
     * {
     *   "statusCode": 404,
     *   "error": "Not Found",
     *   "message": "email not exists",
     *   "code": 10
     * }
     * @apiErrorExample Error password required:
     * {
     *   "statusCode": 422,
     *   "error": "Unprocessable Entity",
     *   "message": "\"password\" is required",
     *   "code": 1
     * }
     * @apiErrorExample Error password is not allowed to be empty:
     * {
     *   "statusCode": 422,
     *   "error": "Unprocessable Entity",
     *   "message": "\"password\" is not allowed to be empty",
     *   "code": 1
     * }
     * @apiErrorExample Error password length must be at least 6 characters long:
     * {
     *   "statusCode": 422,
     *   "error": "Unprocessable Entity",
     *   "message": "\"password\" length must be at least 6 characters long",
     *   "code": 1
     * }
     * @apiErrorExample Error password length must be less than or equal to 255 characters long:
     * {
     *   "statusCode": 422,
     *   "error": "Unprocessable Entity",
     *   "message": "\"password\" length must be less than or equal to 255 characters long",
     *   "code": 1
     * }
     * @apiErrorExample Error email or password incorrect:
     * {
     *   "statusCode": 400,
     *   "error": "Bad Request",
     *   "message": "email or password incorrect",
     *   "code": 11
     * }
     * @apiErrorExample Error deviceToken required:
     * {
     *   "statusCode": 422,
     *   "error": "Unprocessable Entity",
     *   "message": "\"deviceToken\" is required",
     *   "code": 1
     * }
     * @apiErrorExample Error deviceToken is not allowed to be empty:
     * {
     *   "statusCode": 422,
     *   "error": "Unprocessable Entity",
     *   "message": "\"deviceToken\" is not allowed to be empty",
     *   "code": 1
     * }
     * @apiErrorExample Error deviceType required:
     * {
     *   "statusCode": 422,
     *   "error": "Unprocessable Entity",
     *   "message": "\"deviceType\" is required",
     *   "code": 1
     * }
     * @apiErrorExample Error deviceType is not allowed to be empty:
     * {
     *   "statusCode": 422,
     *   "error": "Unprocessable Entity",
     *   "message": "\"deviceType\" is not allowed to be empty",
     *   "code": 1
     * }
     * @apiErrorExample Error login fail:
     * {
     *   "statusCode": 400,
     *   "message": "Bad Request",
     *   "error": "Bad Request",
     *   "code": 2
     * }
     * @apiErrorExample Error server:
     * {
     *   "statusCode": 500,
     *   "message": "An internal server error occurred",
     *   "error": "Internal Server Error",
     *   "code": 3
     * }
     */
}
