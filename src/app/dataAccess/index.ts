import * as mongoose from 'mongoose'
import * as bluebird from 'bluebird'
import * as util from 'util'
import EnvironmentConstant from '../../config/constants/environment.constant'
import spirit from './../../config/debug'

(<any>mongoose).Promise = bluebird

class Index {
    static mongooseInstance: any;
    static mongooseConnection: mongoose.Connection;

    constructor () {
    Index.connect()
  }

    static connect (): mongoose.Connection {
    if (this.mongooseInstance) return this.mongooseInstance

    this.mongooseConnection = mongoose.connection
    this.mongooseConnection.once('open', () => {
      spirit.log('Connected to mongodb.')
    })
    this.mongooseConnection.on('error', (err) => {
      spirit.error(`⚡️ 🚨 ⚡️ 🚨 ⚡️ 🚨 ⚡️ 🚨 ⚡️ 🚨  → ${err.message}`)
      throw err
    })

    if (EnvironmentConstant.env === 'development') {
      mongoose.set('debug', (collectionName, method, query, doc) => {
        spirit.log(`${collectionName}.${method}`, util.inspect(query, false, 20), doc)
      })
    }

    this.mongooseInstance = mongoose
    mongoose.connect(EnvironmentConstant.DB_CONNECTION_STRING, { useNewUrlParser: true })
    return this.mongooseInstance
  }
}

Index.connect()
export default Index
