import Index from '../index'
import UserModelInterface from '../../model/interfaces/user-model.interface'
import BcryptService from '../../services/bcrypt.service'

const mongoose = Index.mongooseInstance
const mongooseConnection = Index.mongooseConnection

class UserSchema {
  static get schema () {
    const schema = mongoose.Schema({
      username: {
        type: String,
        required: true
      },
      email: {
        type: String,
      },
      password: {
        type: String,
        required: true
      }
    })

    schema.pre('save', async function save (next) {
      try {
        const user = this
        if (!user.isModified('password')) return next()

        user.password = await BcryptService.hash(user.password) // hash password

        return next()
      } catch (error) {
        return next(error)
      }
    })

    return schema
  }
}

const schema = mongooseConnection.model<UserModelInterface>('User', UserSchema.schema)
export default schema
