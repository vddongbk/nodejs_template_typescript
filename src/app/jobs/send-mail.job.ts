import * as kue from 'kue'
import Queue from '../../config/queue'
import Spirit from '../../config/debug'
import MailBusinness from '../../app/business/mail.business'

class SendMailJob {
  private _queue

  constructor () {
    this._queue = new Queue()._queue
  }

  sendMail (data) {
    const job = this._queue.create('email', data)
      .priority('high')
      .attempts(5)
      .backoff(true)
      .removeOnComplete(true)
      .on('start', () => {
        Spirit.log('Job', job.id, 'of type', job.data.type, 'is start')
      })
      .on('progress', () => {
        Spirit.log('Job', job.id, 'of type', job.data.type, 'is progress')
      })
      .on('remove', () => {
        Spirit.log('Job', job.id, 'of type', job.data.type, 'is remove')
      })
      .on('complete', () => {
        Spirit.log('Job', job.id, 'of type', job.data.type, 'is done')
      })
      .on('failed', () => {
        Spirit.log('Job', job.id, 'of type', job.data.type, 'has failed')
      })
      .save((err) => {
        if (err) {
          Spirit.log(`create job err: ${job.id}`)
        }
      })

    this._queue.process('email', (jobProcess, done) => {
      try {
        const type = jobProcess.data.type || 'example'
        const mailBusinness = new MailBusinness()
        mailBusinness[type](jobProcess.data.to)
        return done && done()
      } catch (error) {
        Spirit.error(error)
        return done && done(error)
      }
    })

    // others are active, complete, failed, delayed
    // you may want to fetch each id to get the Job object out of it...
    // eslint-disable-next-line
    this._queue.failed((err, ids) => {
      ids.forEach((id) => {
        kue.Job.get(id, (err, job) => {
          job.inactive()
        })
      })
    })
  }
}
export default SendMailJob
