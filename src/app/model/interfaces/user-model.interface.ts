import mongoose = require('mongoose');

interface UserModelInterface extends mongoose.Document {
  username: string;
  email: string;
  password: string;
}

export default UserModelInterface
