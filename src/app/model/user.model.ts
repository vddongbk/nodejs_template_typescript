import UserModelInterface from './interfaces/user-model.interface'

class UserModel {
   private _UserModel: UserModelInterface;

   constructor (UserModel: UserModelInterface) {
    this._UserModel = UserModel
  }

   get username (): string {
    return this._UserModel.username
  }

   get email (): string {
    return this._UserModel.email
  }

   get password (): string {
    return this._UserModel.password
  }
}
Object.seal(UserModel)
export default UserModel
