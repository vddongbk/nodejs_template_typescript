interface ReadInterface<T> {
  findAll: (conditions: object, projection: object | string, options: object,
            callback: (error: any, result: any) => void) => void
  findById: (id: object | string | number, projection: object | string, options: object,
             callback: (error:any, result: T) => void) => void
  findOne: (conditions: object, projection: object | string, options: object,
            callback: (error: any, result: T) => void) => void
  count: (filter: object, callback: (error:any, result: number) => void) => void
  countDocuments: (filter: object, callback: (error:any, result: number) => void) => void
  geoSearch: (conditions: object, options: object, callback: (error: any, result: any) => void) => void
}

export default ReadInterface
