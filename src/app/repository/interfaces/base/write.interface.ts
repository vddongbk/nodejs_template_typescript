interface WriteInterface<T> {
  create: (item:T, options: object, callback: (error: any, result: any) => void) => void
  insertMany: (items: any[], callback: (error: any, result: any) => void) => void
  update: (conditions: object, item:T, options: object, callback: (error: any, result: any)=> void) => void
  findByIdAndRemove: (_id: string, options: object | null, callback: (error: any, result: any) => void) => void
  findByIdAndUpdate: (_id: object | string | number, item:T, projection: object | string, options: object,
                      callback: (error: any, result: any) => void) => void
  findOneAndUpdate: (conditions: object, item:T, projection: object | string, options: object,
                     callback: (error: any, result: any) => void) => void
  remove: (conditions: object, callback: (error: any, result: any) => void) => void
  deleteMany: (conditions: object, callback: (error: any, result: any) => void) => void
  deleteOne: (conditions: object, callback: (error: any, result: any) => void) => void
}

export default WriteInterface
