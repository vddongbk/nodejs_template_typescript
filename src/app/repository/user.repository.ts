import UserModelInterface from '../model/interfaces/user-model.interface'
import UserSchema from '../dataAccess/schemas/user.schema'
import BaseRepository from './base/base.repository'

class UserRepository extends BaseRepository<UserModelInterface> {
  constructor () {
    super(UserSchema)
  }
}

Object.seal(UserRepository)
export default UserRepository
