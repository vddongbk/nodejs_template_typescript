import ReadInterface from '../interfaces/base/read.interface'
import WriteInterface from '../interfaces/base/write.interface'
import { optionGeoSearch } from '../../../config/types'

import mongoose = require('mongoose');

class BaseRepository<T extends mongoose.Document> implements ReadInterface<T>, WriteInterface<T> {
  private _model: mongoose.Model<mongoose.Document>

  constructor (schemaModel: mongoose.Model<mongoose.Document>) {
    this._model = schemaModel
  }

  findAll (conditions: object, projection: object | string, options: object,
    callback: (error: any, result: any) => void) {
    this._model.find(conditions, callback)
  }

  findById (_id: object | string | number, projection: object | string, options: object,
    callback: (error: any, result: T) => void) {
    this._model.findById(_id, callback)
  }

  findOne (conditions: object, projection: object | string, options: object,
    callback: (error: any, result: T) => void) {
    this._model.findOne(conditions, projection, options, callback)
  }

  count (filter: object, callback: (error: any, result: number) => void) {
    this._model.count(filter, callback)
  }

  countDocuments (filter: object, callback: (error: any, result: number) => void) {
    this._model.countDocuments(filter, callback)
  }

  geoSearch (conditions: object, options: optionGeoSearch, callback: (error: any, result: any) => void) {
    this._model.geoSearch(conditions, options, callback)
  }

  create (item: T, options: object, callback: (error: any, result: any) => void): any {
    this._model.create(item, options, callback)
  }

  insertMany (items: any[], callback: (error: any, result: any) => any) {
    this._model.insertMany(items, callback)
  }

  update (conditions: object, item: T, options: object, callback: (error: any, result: any) => void) {
    this._model.update(conditions, item, options, callback)
  }

  findByIdAndUpdate (_id: string, item: T, projection: object | string, options: object,
    callback:(error: any, result: any) => void) {
    this._model.findByIdAndUpdate({ _id: this.toObjectId(_id) }, callback)
  }

  findOneAndUpdate (conditions: object, item: T, projection: object | string, options: object,
    callback: (error: any, result: any) => void) {
    this._model.update(conditions, item, callback)
  }

  findByIdAndRemove (_id: string, options: object| null, callback:(error: any, result: any) => void) {
    this._model.findByIdAndRemove(this.toObjectId(_id), options, (err) => callback(err, null))
  }

  remove (conditions: object, callback: (error: any, result: any) => void) {
    this._model.remove(conditions, (err) => callback(err, null))
  }

  deleteMany (conditions: object, callback: (error: any, result: any) => void) {
    this._model.deleteMany(conditions, (err) => callback(err, null))
  }

  deleteOne (conditions: object, callback: (error: any, result: any) => void) {
    this._model.deleteOne(conditions, (err) => callback(err, null))
  }

  private toObjectId (_id: string) : mongoose.Types.ObjectId {
    return mongoose.Types.ObjectId.createFromHexString(_id)
  }
}

export default BaseRepository
