import BaseBusinessInterface from './base/base-business.interface'
import UserModelInterface from '../../model/interfaces/user-model.interface'

interface UserBusinessInterface extends BaseBusinessInterface<UserModelInterface> {

}

export default UserBusinessInterface
