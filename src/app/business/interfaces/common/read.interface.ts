interface ReadInterface<T> {
    findAll: (conditions: object, projection: object | string | null, options: object | null)=> void
    findById: (_id: string, projection: object | string | null, options: object | null) => void
    findOne: (conditions: object, projection: object | string | null, options: object | null) => void
}

export default ReadInterface
