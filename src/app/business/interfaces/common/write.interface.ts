interface WriteInterface<T> {
    create: (item: T, options: object | null) => void
    update:(conditions: object, item: T, options: object | null) => void
    delete: (_id: string) => void
    remove: (conditions: object) => void
}

export default WriteInterface
