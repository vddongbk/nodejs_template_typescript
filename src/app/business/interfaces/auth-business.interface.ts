interface AuthBusinessInterface {
    login: (email: string, password: string) => void
}

export default AuthBusinessInterface
