import ReadInterface from '../common/read.interface'
import WriteInterface from '../common/write.interface'

interface BaseBusinessInterface<T> extends ReadInterface<T>, WriteInterface<T>
{
}
export default BaseBusinessInterface
