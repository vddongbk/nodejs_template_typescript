import UserRepository from '../repository/user.repository'
import AuthBusinessInterface from './interfaces/auth-business.interface'
import BcryptService from '../services/bcrypt.service'

class AuthBusiness implements AuthBusinessInterface {
    private _userRepository: UserRepository;

    constructor () {
    this._userRepository = new UserRepository()
  }

    login (email: string, password: string): Promise<any> {
    return new Promise((resolve, reject) => {
      const select = '_id email password'
      this._userRepository.findOne({ email }, select, { lean: true }, async (error: any, result: any) => {
        if (error) return reject(error)
        const user = result
        const isPasswordCorrect = await BcryptService.compare(password, user.password)
        if (!isPasswordCorrect) return null

        delete user.password
        return resolve(user)
      })
    })
  }
}

Object.seal(AuthBusiness)
export default AuthBusiness
