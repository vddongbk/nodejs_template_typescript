/**
 * Email Service
 * @description Server-side logic for Email handle
 */
import * as assert from 'assert'
import MailService from '../services/mail.service'
import MailBusinessInterface from './interfaces/mail-business.interface'

class MailBusiness implements MailBusinessInterface {
  example (email: string, type: string = 'gmail'): Promise<any> {
    assert(email, 'EmailService - "email" is required')
    const options = {
      to: email,
      from: 'Example <info@example.net>',
      subject: 'Example title',
      html: '<h3>Hello</h3>'
    }

    if (type === 'ses') {
      return MailService.sendBySES(options)
    }
    return MailService.sendByGmail(options)
  }
}

export default MailBusiness
