import UserRepository from '../repository/user.repository'
import UserBusinessInterface from './interfaces/user-business.interface'
import IUserModel from '../model/interfaces/user-model.interface'

class UserBusiness implements UserBusinessInterface {
  private _userRepository: UserRepository;

  constructor () {
    this._userRepository = new UserRepository()
  }

  create (item: IUserModel, options?: object | null) {
    return new Promise((resolve, reject) => {
      this._userRepository.create(item, options, (error: any, result: any) => {
        if (error) reject(error)
        resolve(result)
      })
    })
  }

  findAll (conditions: object, projection?: object | string | null, options?: object | null) {
    return new Promise((resolve, reject) => {
      this._userRepository.findAll(conditions, projection, options, (error: any, result: any) => {
        if (error) reject(error)
        resolve(result)
      })
    })
  }

  update (conditions: object, item: IUserModel, options?: object | null) {
    return new Promise((resolve, reject) => {
      this._userRepository.update(conditions, item, options, (error: any, result: any) => {
        if (error) reject(error)
        resolve(result)
      })
    })
  }

  delete (_id: string) {
    return new Promise((resolve, reject) => {
      this._userRepository.findByIdAndRemove(_id, null, (error: any, result: any) => {
        if (error) reject(error)
        resolve(result)
      })
    })
  }

  findById (_id: string, projection?: object | string | null, options?: object | null) {
    return new Promise((resolve, reject) => {
      this._userRepository.findById(_id, projection, options, (error: any, result: any) => {
        if (error) reject(error)
        resolve(result)
      })
    })
  }

  findOne (conditions: object, projection?: object | string | null, options?: object | null) {
    return new Promise((resolve, reject) => {
      this._userRepository.findOne(conditions, projection, options, (error: any, result: any) => {
        if (error) reject(error)
        resolve(result)
      })
    })
  }

  remove (conditions?: object) {
    return new Promise((resolve, reject) => {
      this._userRepository.remove(conditions, (error: any, result: any) => {
        if (error) reject(error)
        resolve(result)
      })
    })
  }
}

Object.seal(UserBusiness)
export default UserBusiness
