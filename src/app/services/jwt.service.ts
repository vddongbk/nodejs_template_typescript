import * as jwt from 'jsonwebtoken'
import * as assert from 'assert'
import Environment from '../../config/constants/environment.constant'
import JwtServiceInterface from './interfaces/jwt-service.interface'

class JwtService implements JwtServiceInterface {
  generateToken (data: any, secret: string = Environment.jwtSecret, expire: string = Environment.jwtExpireTime)
    : string {
    assert(data, 'JWTService - "data" is required')
    const jwtSign = jwt.sign(data, secret, { expiresIn: expire })
    return jwtSign
  }

  jwtVerify (token: string, secret: string = Environment.jwtSecret): Promise<any> {
    assert(token, 'JWTService - "token" is required')
    const jwtVerify = new Promise((resolve, reject) => {
      jwt.verify(token, secret, (error, decode) => {
        if (error) reject(error)
        resolve(decode)
      })
    })
    return jwtVerify
  }
}

export default new JwtService()
