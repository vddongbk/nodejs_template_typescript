import * as bcrypt from 'bcrypt-nodejs'
import * as assert from 'assert'
import Spirit from '../../config/debug'
import BcryptServiceInterface from './interfaces/bcrypt-service.interface'

class BcryptService implements BcryptServiceInterface {
  hash (originPassword: string): Promise<any> {
    assert(originPassword, 'BcryptService - "origin" is required')
    const promise = new Promise((resolve, reject) => {
      bcrypt.genSalt(10, (err, salt) => {
        if (err) reject(err)
        bcrypt.hash(originPassword, salt, null, (newErr, hash) => {
          if (newErr) {
            Spirit.error('BcryptService - hash method', newErr)
            reject(newErr)
          } else resolve(hash)
        })
      })
    })
    return promise
  }

  compare (origin: string, hash: string): Promise<any> {
    assert(origin, 'BcryptService - "origin" is required')
    assert(hash, 'BcryptService - "hash" is required')
    const promise = new Promise((resolve, reject) => {
      bcrypt.compare(origin, hash, (err, result) => {
        if (err) {
          Spirit.error('BcryptService - compare method', err)
          reject(err)
        } else resolve(result)
      })
    })
    return promise
  }
}

export default new BcryptService()
