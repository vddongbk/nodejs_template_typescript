import * as nodemailer from 'nodemailer'
import * as assert from 'assert'
import * as sesTransport from 'nodemailer-ses-transport'
import Environment from '../../config/constants/environment.constant'
import MailServiceInterface from './interfaces/mail-service.interface'

/**
*
* @param {object} options
*    {string} to        : email address of receiver
*    {string} from      : email address of sender < e.g noreply@tokuby.vn >
*    {string} subject   : title of email
*    {string} html      : content of email in html format
* @return {Promise}
 */

class MailService implements MailServiceInterface {
  sendByGmail (options: nodemailer.SendMailOptions): Promise<any> {
    assert(options, 'GmailService - "options" is required')
    assert(options.from, 'GmailService - "from" is required')
    assert(options.to, 'GmailService - "to" is required')
    assert(options.subject, 'GmailService - "subject" is required')
    assert(options.html, 'GmailService - "html" is required')
    const promise = new Promise((resolve, reject) => {
      const { from, to, subject, html } = options
      if (!from) throw new Error('GmailService - Require options.from property')
      if (!to) throw new Error('GmailService - Require options.to property')
      if (!subject) throw new Error('GmailService - Require options.subject property')
      if (!html) throw new Error('GmailService - Require options.html property')

      const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: Environment.gmail
      })

      const mailOptions : nodemailer.SendMailOptions = {
        from: options.from,
        to: options.to,
        subject: options.subject,
        html: options.html
      }

      transporter.sendMail(mailOptions, (error, info) => {
        if (error) reject(error)
        resolve(info)
      })
    })
    return promise
  }

  sendBySES (options: nodemailer.SendMailOptions): Promise<any> {
    assert(options, 'GmailService - "options" is required')
    assert(options.from, 'GmailService - "from" is required')
    assert(options.to, 'GmailService - "to" is required')
    assert(options.subject, 'GmailService - "subject" is required')
    assert(options.html, 'GmailService - "html" is required')
    const promise = new Promise((resolve, reject) => {
      const { from, to, subject, html } = options
      if (!from) throw new Error('SESService - Require options.from property')
      if (!to) throw new Error('SESService - Require options.to property')
      if (!subject) throw new Error('SESService - Require options.subject property')
      if (!html) throw new Error('SESService - Require options.html property')

      const transporter = nodemailer.createTransport(sesTransport(Environment.ses))

      const mailOptions : nodemailer.SendMailOptions = {
        from: options.from,
        to: options.to,
        subject: options.subject,
        html: options.html
      }

      transporter.sendMail(mailOptions, (error, info) => {
        if (error) reject(error)
        resolve(info)
      })
    })
    return promise
  }
}

export default new MailService()
