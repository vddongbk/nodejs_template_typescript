interface HttpServiceInterface {
  get: (uri: string, token: string) => void
  post: (uri: string, token: string, formData: object) => void
  postAsync (uri: string, data: any): Promise<any>
  getAsyncByToken (uri: string, token: string): Promise<any>
}

export default HttpServiceInterface
