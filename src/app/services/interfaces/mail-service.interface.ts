import * as nodemailer from 'nodemailer'

interface MailServiceInterface {
    sendByGmail: (options: nodemailer.SendMailOptions) => Promise<any>
    sendBySES: (options: nodemailer.SendMailOptions) => Promise<any>
}

export default MailServiceInterface
