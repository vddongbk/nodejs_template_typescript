import { Request } from 'express'
import { uploadOptions } from '../../../config/types'

interface UploadServiceInterface {
    upload: (req: Request, nameField: string, options: uploadOptions) => void
}

export default UploadServiceInterface
