interface RedisServiceInterface {
    get: (key: string) => Promise<any>
    stringsSet: (key: string, data: any, expiresIn: string) => Promise<any>
    stringsSetNoExpires: (key: string, data: any) => Promise<any>
    delete: (key: string) => Promise<any>
    increment: (key: string) => Promise<any>
    getKeyRemainingTime: (key: string) => Promise<any>
    checkExist: (key: string) => Promise<any>
    sadd: (key: string, data: any) => Promise<any>
    smembers: (key: string) => Promise<any>
    spop: (key:string, amount: number) => Promise<any>
    scard: (key: string) => Promise<any>
    sort: (key: string, sort: string, limit: number) => Promise<any>
    clean: () => Promise<any>
}

export default RedisServiceInterface
