interface JwtServiceInterface {
    generateToken: (data: any, secret: string, expire: string) => void
    jwtVerify: (token: string, secret: string) => void
}

export default JwtServiceInterface
