interface BcryptServiceInterface {
    hash: (originPassword: string) => void
    compare: (origin: string, hash: string) => void
}

export default BcryptServiceInterface
