import * as redis from 'redis'
import * as assert from 'assert'
import { promisify } from 'util'
import Environment from '../../config/constants/environment.constant'
import RedisServiceInterface from './interfaces/redis-service.interface'

const client = redis.createClient(Environment.redis)

const redisGet : Function = promisify(client.get).bind(client)
const redisSet : Function = promisify(client.set).bind(client)
const redisDel : Function = promisify(client.del).bind(client)
const redisIncr : Function = promisify(client.incr).bind(client)
const redisTtl : Function = promisify(client.ttl).bind(client)
const redisExists : Function = promisify(client.exists).bind(client)
const redisSadd : Function = promisify(client.sadd).bind(client)
const redisSmembers : Function = promisify(client.smembers).bind(client)
const redisSpop : Function = promisify(client.spop).bind(client)
const redisScard : Function = promisify(client.scard).bind(client)
const redisSort : Function = promisify(client.sort).bind(client)
const redisClean : Function = promisify(client.flushall).bind(client)

class RedisService implements RedisServiceInterface {
  /**
  *
  * @param key
  * @returns {Promise<any>}
     */
  get (key: string): Promise<any> {
    assert(key, 'Redis service - method get - "key" is require')
    return redisGet(key)
  }

  /**
  *
  * @param key
  * @param data
  * @param expiresIn
  * @returns {Promise<any>}
     */
  stringsSet (key: string, data: any, expiresIn: string): Promise<any> {
    assert(key, 'Redis service - method stringSet - "key" is require')
    assert(data, 'Redis service - method stringSet - "data" is require')
    return redisSet(key, data, 'EX', expiresIn)
  }

  /**
  *
  * @param key
  * @param data
  * @returns {Promise<any>}
     */
  stringsSetNoExpires (key: string, data: any): Promise<any> {
    assert(key, 'Redis service - method stringsSetNoExpires - "key" is require')
    assert(data, 'Redis service - method stringsSetNoExpires - "data" is require')
    return redisSet(key, data)
  }

  /**
  *
  * @param key
  * @returns {Promise<any>}
     */
  delete (key: string): Promise<any> {
    assert(key, 'Redis service - method delete - "key" is require')
    return redisDel(key)
  }

  /**
  *
  * @param key
  * @returns {Promise<any>}
     */
  increment (key: string): Promise<any> {
    assert(key, 'Redis service - method increment - "key" is require')
    return redisIncr(key)
  }

  /**
  *
  * @param key
  * @returns {Promise<any>}
     */
  getKeyRemainingTime (key: string): Promise<any> {
    assert(key, 'Redis service - method getKeyRemainingTime - "key" is require')
    return redisTtl(key)
  }

  /**
  *
  * @param key
  * @returns {Promise<any>}
     */
  checkExist (key: string): Promise<any> {
    assert(key, 'Redis service - method checkExist - "key" is require')
    return redisExists(key)
  }

  /**
  *
  * @param key
  * @param data
  * @returns {Promise<any>}
     */
  sadd (key: string, data: any): Promise<any> {
    assert(key, 'Redis service - method sadd - "key" is require')
    assert(data, 'Redis service - method sadd - "data" is require')
    return redisSadd(key, data)
  }

  /**
  *
  * @param key
  * @returns {Promise<any>}
     */
  smembers (key: string): Promise<any> {
    assert(key, 'Redis service - method smembers - "key" is require')
    return redisSmembers(key)
  }

  /**
  * Remove and return one or multiple random members from a set
  * @param key: redis key
  * @param amount: number of members
  * @return {Promise.<array>} array members removed
     */
  spop (key:string, amount: number): Promise<any> {
    assert(key, 'Redis service - method spop - "key" is require')
    assert(amount, 'Redis service - method spop - "amount" is require')
    return redisSpop(key, amount)
  }

  scard (key: string): Promise<any> {
    assert(key, 'Redis service - method scard - "key" is require')
    return redisScard(key)
  }

  /**
  *
  * @param key
  * @param sort
  * @param limit
  * @returns {Promise<any>}
     */
  sort (key: string, sort: string = 'DESC', limit: number = 5): Promise<any> {
    assert(key, 'Redis service - method sort - "key" is require')
    return redisSort(key, sort, 'by', 'total_*', 'limit', 0, limit)
  }

  clean (): Promise<any> {
    return redisClean()
  }
}

export default new RedisService()
