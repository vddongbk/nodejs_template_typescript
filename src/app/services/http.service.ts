import * as request from 'request'
import * as assert from 'assert'
import fetch, { Headers, Request, RequestInit, Response } from 'node-fetch'
import HttpServiceInterface from './interfaces/http-service.interface'

class HttpService implements HttpServiceInterface {
  get (uri: string, token?: string | null): Promise<any> {
    assert(uri, 'HttpService - "uri" is required')
    const promise = new Promise((resolve, reject) => {
      const options : any = {
        method: 'get',
        uri,
        json: true
      }

      if (token) {
        options.headers = {
          Authorization: `Bearer ${token}`
        }
      }

      request.get(options, (error, response, body) => {
        if (error) reject(error)
        else resolve(body)
      })
    })
    return promise
  }

  post (uri: string, token?: string, formData?: object): Promise<any> {
    assert(uri, 'HttpService - "uri" is required')
    const promise = new Promise((resolve, reject) => {
      const options : any = {
        method: 'post',
        uri,
        json: true
      }

      if (formData) {
        options.form = formData
      }

      if (token) {
        options.headers = {
          Authorization: `Bearer ${token}`
        }
      }
      request.post(options, (error, response, body) => {
        if (error) reject(error)
        else resolve(body)
      })
    })
    return promise
  }

  async postAsync (uri: string, data?: any): Promise<any> {
    try {
      const options : RequestInit = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      }
      const res = await fetch(uri, options)
      return await res.json()
    } catch (error) {
      throw error
    }
  }

  async getAsyncByToken (uri: string, token?: string): Promise<any> {
    try {
      const options : RequestInit = {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      }
      const res = await fetch(uri, options)
      return await res.json()
    } catch (error) {
      throw error
    }
  }
}

export default new HttpService()
