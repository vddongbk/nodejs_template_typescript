import { Request, Response } from 'express'
import UserBusiness from '../business/user.business'
import IBaseController from './interfaces/base/base.controller'
import UserModelInterface from '../model/interfaces/user-model.interface'
import HttpResponse from '../../config/responses'

class UserController implements IBaseController <UserBusiness> {
  async create (req: Request, res: Response): Promise<any> {
    const User : UserModelInterface = <UserModelInterface>req.body
    const userBusiness = new UserBusiness()
    const result = await userBusiness.create(User)
    if (!result) { HttpResponse.badRequest(res) }
    HttpResponse.createdSuccess(res, { data: result, message: 'success' })
  }

  async update (req: Request, res: Response): Promise<any> {
    const User : UserModelInterface = <UserModelInterface>req.body
    const _id : string = req.params._id
    const userBusiness = new UserBusiness()
    const result = await userBusiness.update({ _id }, User)
    if (!result) { HttpResponse.badRequest(res) }
    HttpResponse.success(res, { data: result, message: 'success' })
  }

  async delete (req: Request, res: Response): Promise<any> {
    const _id : string = req.params._id
    const userBusiness = new UserBusiness()
    await userBusiness.delete(_id)
    HttpResponse.success(res, { data: null, message: 'success' })
  }

  async retrieve (req: Request, res: Response): Promise<any> {
    const userBusiness = new UserBusiness()
    const result = await userBusiness.findAll({})
    HttpResponse.success(res, { data: result, message: 'success' })
  }

  async findById (req: Request, res: Response): Promise<any> {
    const _id : string = req.params._id
    const userBusiness = new UserBusiness()
    const result = await userBusiness.findById(_id)
    if (!result) { HttpResponse.badRequest(res) }
    HttpResponse.success(res, { data: result, message: 'success' })
  }
}

export default UserController
