import { Request, Response } from 'express'
import AuthControllerAbstract from './abstracts/auth-base.controller'
import HttpResponse from '../../config/responses'

class AuthController extends AuthControllerAbstract {
  async healthCheck (req: Request, res: Response): Promise<any> {
    return HttpResponse.success(res)
  }
}

export default AuthController
