import ReadControllerInterface from '../common/read-controller.interface'
import WriteControllerInterface from '../common/write-controller.interface'
import IBaseBusiness from '../../../business/interfaces/base/base-business.interface'

interface BaseController<T extends IBaseBusiness<Object>> extends ReadControllerInterface, WriteControllerInterface{

}
export default BaseController
