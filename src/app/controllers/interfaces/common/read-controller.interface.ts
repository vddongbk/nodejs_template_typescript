import { RequestHandler } from 'express'

interface ReadControllerInterface {
    retrieve: RequestHandler;
    findById: RequestHandler;
}

export default ReadControllerInterface
