import { RequestHandler } from 'express'

interface AuthInterface {
  login: RequestHandler
  loginByFacebook: RequestHandler
  loginByGoogle: RequestHandler
  loginByZalo: RequestHandler
}

export default AuthInterface
