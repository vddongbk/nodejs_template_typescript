import { RequestHandler } from 'express'

interface WriteControllerInterface {
    create: RequestHandler;
    update: RequestHandler;
    delete: RequestHandler;
}

export default WriteControllerInterface
