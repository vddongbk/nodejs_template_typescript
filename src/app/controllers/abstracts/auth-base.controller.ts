import { Request, Response } from 'express'
import UserModelInterface from '../../model/interfaces/user-model.interface'
import AuthBusiness from '../../business/auth.business'
import UserBusiness from '../../business/user.business'
import HttpResponse from '../../../config/responses'
import JwtService from '../../services/jwt.service'
import Environment from '../../../config/constants/environment.constant'
import HttpService from '../../services/http.service'
import AuthInterface from '../interfaces/common/auth.interface'

abstract class AuthBaseController implements AuthInterface {
  // login by email
  async login (req: Request, res: Response): Promise<any> {
    const data : UserModelInterface = <UserModelInterface>req.body
    const authBusiness = new AuthBusiness()
    const { email, password } = data
    const user = await authBusiness.login(email, password)
    if (!user) {
      return HttpResponse.badRequest(res)
    }
    const token = JwtService.generateToken(user)
    return HttpResponse.createdSuccess(res, { data: token, message: 'success' })
  }

  // login by social: Faceboook
  async loginByFacebook (req: Request, res: Response) {
    const { accessToken } = req.body
    const dataFB = await HttpService.get(`${Environment.fbUrl}${accessToken}`)
    if (dataFB && dataFB.error) {
      return HttpResponse.badRequestMessage(res, dataFB.error.message)
    }
    const userBusiness = new UserBusiness()
    const userByFaceBook = await userBusiness
      .findOne({ facebookId: dataFB.id }, '_id email')
    if (!userByFaceBook) {
      const token = JwtService.generateToken(userByFaceBook)
      return HttpResponse.createdSuccess(res, { data: token, message: 'success' })
    }
    const data : any = {
      email: dataFB.email,
      username: dataFB.name,
      gender: dataFB.gender,
      birthday: dataFB.birthday,
      facebookId: dataFB.id,
      avatar: {
        origin: dataFB.picture.data.url,
        thumb: dataFB.picture.data.url,
      }
    }
    const user = await userBusiness.create(data)
    const token = JwtService.generateToken(user)
    return HttpResponse.createdSuccess(res, { data: token, message: 'success' })
  }

  // login by social: Google
  async loginByGoogle (req: Request, res: Response) {
    const { accessToken } = req.body
    const dataGoogle = await HttpService.getAsyncByToken(`${Environment.ggUrl}`, accessToken)
    if (dataGoogle && dataGoogle.error) {
      return HttpResponse.badRequestMessage(res, dataGoogle.error.message)
    }
    const userBusiness = new UserBusiness()
    const userByGoogle = await userBusiness
      .findOne({ googleId: dataGoogle.id }, '_id email')
    if (!userByGoogle) {
      const token = JwtService.generateToken(userByGoogle)
      return HttpResponse.createdSuccess(res, { data: token, message: 'success' })
    }
    const data : any = {
      email: dataGoogle.email,
      username: dataGoogle.name,
      googleId: dataGoogle.id,
      avatar: {
        origin: dataGoogle.picture,
        thumb: dataGoogle.picture,
      }
    }

    const user = await userBusiness.create(data)
    const token = JwtService.generateToken(user)
    return HttpResponse.createdSuccess(res, { data: token, message: 'success' })
  }

  // login by social: Zalo
  async loginByZalo (req: Request, res: Response) {
    const { accessToken } = req.body
    const dataZalo = await HttpService.get(`${Environment.zaloUrl}${accessToken}`)
    if (dataZalo && dataZalo.error) {
      return HttpResponse.badRequestMessage(res, 'access_token_invalid')
    }
    const userBusiness = new UserBusiness()
    const userByZalo = await userBusiness
      .findOne({ zaloId: dataZalo.id }, '_id email')
    if (!userByZalo) {
      const token = JwtService.generateToken(userByZalo)
      return HttpResponse.createdSuccess(res, { data: token, message: 'success' })
    }
    const data : any = {
      username: dataZalo.name,
      zaloId: dataZalo.id,
      gender: dataZalo.gender,
      birthday: dataZalo.birthday,
      avatar: {
        origin: dataZalo.picture.data.url,
        thumb: dataZalo.picture.data.url,
      }
    }

    const user = await userBusiness.create(data, null)
    const token = JwtService.generateToken(user)
    return HttpResponse.createdSuccess(res, { data: token, message: 'success' })
  }
}

export default AuthBaseController

