import * as validate from 'express-validation'
import * as BaseJoi from 'joi'
import * as Extension from 'joi-date-extensions'
import * as ExtensionPhone from 'joi-phone-number'

const JoiPhone = BaseJoi.extend(ExtensionPhone)
const Joi = JoiPhone.extend(Extension)

const validations = {
  login: {
    body: {
      email: Joi.string()
        .trim()
        .lowercase()
        .required()
        .email(),
      password: Joi.string()
        .trim()
        .required()
        .min(6)
        .max(255),
    }
  },
}

class AuthValidation {
    login = validate(validations.login)
}

export default new AuthValidation()
