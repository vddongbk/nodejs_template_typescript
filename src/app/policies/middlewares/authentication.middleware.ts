import { Request, Response, NextFunction } from 'express'
import Spirit from '../../../config/debug'
import HttpResponse from '../../../config/responses'
import asyncWrap from './async-wrap.middleware'
import Helpers from '../../../config/helpers'
import UserBusiness from '../../business/user.business'
import JwtService from '../../services/jwt.service'

class AuthenticationMiddleware {
    authentication = asyncWrap(async (req: Request, res: Response, next: NextFunction) => {
      const userBusiness = new UserBusiness()
      const authorization = req.headers.authorization || null
      let token = null

      if (!authorization) return HttpResponse.unauthorized(res)

      if (authorization && authorization.split(' ')[0] === 'Bearer') {
        token = authorization.split(' ')[1]
      }

      if (!token) return HttpResponse.unauthorized(res)

      const decode = await JwtService.jwtVerify(token)

      const userId = decode && (<any>decode)._id

      if (!Helpers.isMongoId(userId)) {
        return HttpResponse.unauthorized(res)
      }

      (<any>req).user = await userBusiness.findById(userId)

      if (!(<any>req).user) {
        return HttpResponse.unauthorized(res)
      }

      return next()
    }, (req, res, error) => {
      Spirit.error(error)
      return HttpResponse.unauthorized(res)
    })
}

export default new AuthenticationMiddleware()
