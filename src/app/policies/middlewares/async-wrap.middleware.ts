import { Request, Response, NextFunction } from 'express'
import { fn, errorCallback } from '../../../config/types'
import Spirit from '../../../config/debug'
import HttpResponse from '../../../config/responses'
import ErrorsConstant from '../../../config/constants/errors.constant'
import Environment from '../../../config/constants/environment.constant'
import Notify from '../../../config/notify'

class AsyncWrapMiddleware {
  asyncWrap (fn: fn, errCallback?: errorCallback) {
    const asyncWrap = (req: Request, res: Response, next: NextFunction) => {
      fn(req, res, next)
        .catch((error) => {
          if (errCallback) {
            Spirit.error(error)
            if (Environment.env === 'development') { Notify.error('Error!', error) }
            errCallback(req, res, error)
          } else {
            Spirit.error(error)
            if (Environment.env === 'development') { Notify.error('Error!', error) }
            if (error && error.message === 'type not allows') {
              return HttpResponse.badRequest(res, ErrorsConstant.typeNotAllows)
            }
            if (error && error.message === 'file too larger') {
              return HttpResponse.badRequest(res, ErrorsConstant.fileTooLarger)
            }
            if (error && error.message === 'Error in-progress upload') {
              return HttpResponse.badRequest(res, ErrorsConstant.errorhandleUpload)
            }
            if (Environment.env === 'production') {
              return HttpResponse.badRequest(res)
            }
          }
          return HttpResponse.servereError(res)
        })
    }
    return asyncWrap
  }
}

export default new AsyncWrapMiddleware().asyncWrap
