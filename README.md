## Spirit framework for Node.js applications written in TypeScript
**Author:**[@dongvd]()

> *Deployment*

```text
- `npm i` or `yarn`
  Note: `npm install -g yarn` or `npm i npm -g`
- Set environment (vars): `cp .env.example .env` or `npm run env` or `yarn env`
- Start server:
+  `npm build` or `yarn build`
+  `npm start` or `yarn start`
- App run: http://localhost:3001
- Clean build: `npm run clean` or `yarn clean`
- Run server dev: `npm run dev` or `yarn dev`

```

> *Testing*

*Write unit test*

- src/test/

*Run testing*

```text
# Run tests written in ES6 
`npm run test` or `yarn test`
```

> *Documents API*

*Write documents API*

- src/docs/

*Render documents API*

- `apidoc -i src/docs/ -o public/apidoc/` or `npm run doc` or `yarn doc`

*Install apidoc*

- `npm i apidoc -g`

*View documents API*

- Access location: http://localhost:4040/apidoc/

> *Clean Code - Eslint*

```text
# Lint code with ESLint
`npm run lint` or `yarn lint`
```

> *Pre-commit* [husky](https://github.com/typicode/husky)

- Check eslint and test code: `npm run precommit` or `yarn precommit`
- Fix all error and commit.

> *Seeding*

```text
- Write seed and clean db into: src/seed/helpers.js

* production
- Run seed: `npm run migrate` or `yarn migrate`
- Drop dbs: `npm run migrate:drop` or `yarn migrate:drop`

* devlopment
- Run seed dev: `npm run migrate:dev` or `yarn migrate:dev`
- Drop dbs dev: `npm run migrate:dev:drop` or `yarn migrate:dev:drop`

```

> *References*
- https://github.com/ErickWendel/Learning.NodeJSWithTypescript#readme
